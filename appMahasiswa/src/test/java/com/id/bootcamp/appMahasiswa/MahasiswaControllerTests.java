package com.id.bootcamp.appMahasiswa;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.regex.Pattern;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
public class MahasiswaControllerTests {
	
	@Mock
	dataMahasiswa dataMahasiswa;
	
	@InjectMocks
	MahasiswaController mahasiswaController;
	
    @Test
    void testHapusMahasiswa() {
    	
    }

    @Test
    void testTambahMahasiswa() {
    	when(dataMahasiswa.retrieveData()).thenReturn(new Mahasiswa("Bude", 19, "NIM--052"));
        // when(dataMahasiswa.retrieveData()).thenReturn(new Mahasiswa("Budi",18,"NIM--009"));
    	assertEquals("Berhasil", mahasiswaController.tambahMahasiswa());
    }
    @Test
    void testVerifyName() {
    	String name = "Budi";
    	String regex = "[a-zA-Z]+";
    	boolean status;
		if (Pattern.compile(regex).matcher(name).find()) {
//			model.setNama(name);
			status =  true;
		} else {
			status =  false;
		}
		assertEquals(true, status);
    }
}

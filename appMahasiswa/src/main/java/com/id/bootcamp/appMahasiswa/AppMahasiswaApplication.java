package com.id.bootcamp.appMahasiswa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppMahasiswaApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppMahasiswaApplication.class, args);
		Mahasiswa model = new Mahasiswa();
		MahasiswaView view = new MahasiswaView();
		MahasiswaController controller = new MahasiswaController(model,view);
		controller.tampilanMenu();
	}

}

//NIM pake Regex NIM--ANGKA(3)
//NIM--[0-9]{3}
//Nama gaboleh ada angka
/*[^0-9]
 * umur diatas 17
 * */


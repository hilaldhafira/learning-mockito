package com.id.bootcamp.appMahasiswa;

public class Mahasiswa {
	private String nama;
	private int umur;
	private String nim;
	
	public Mahasiswa() {
		
	}
	
	public Mahasiswa(String nama, int umur, String nim) {
		this.nama = nama;
		this.umur = umur;
		this.nim = nim;
	}	
	public String getNama() {
		return nama;
	}
	public String getNim() {
		return nim;
	}
	public int getUmur() {
		return umur;
	}
	public void setNama(String name) {
		this.nama = name;
	}
	public void setUmur(int age) {
		this.umur = age;
	}
	public void setNim(String nim) {
		this.nim = nim;
	}
}

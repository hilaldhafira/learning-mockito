package com.id.bootcamp.appMahasiswa;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;

public class MahasiswaController {
	
	dataMahasiswa dm;
	Mahasiswa model = new Mahasiswa();
	MahasiswaView view = new MahasiswaView();
	ArrayList<Mahasiswa> mhs = new ArrayList<Mahasiswa>();
	Scanner input = new Scanner(System.in);


	
	public MahasiswaController(Mahasiswa model, MahasiswaView view) {
		this.model = model;
		this.view = view;
	}
	public MahasiswaController(Mahasiswa model, MahasiswaView view, dataMahasiswa dm) {
		this.model = model;
		this.view = view;
		this.dm = dm;
	}
	public MahasiswaController() {
	}
	public String tambahMahasiswa() {
		Mahasiswa data = dm.retrieveData();
	
		boolean nama = verifyName(data.getNama());
		boolean nim = verifyNIM(data.getNim());
		boolean age = verifyAge(data.getUmur()) ;
		if (nama == true && nim == true && age == true) {
			mhs.add(data);
			return "Berhasil";
		} else {
			System.out.println("ada gagal");			
			return "Gagal";
		}
		
	}
	public String tampilanMenu() {
		view.menu();
		pilihan(pilihMenu());
		
		return "Berhasil";
	}
	public int pilihMenu() {
		int pil = Integer.parseInt(input.nextLine());
		return pil;
	}
	public String hapusMahasiswa() {
		Mahasiswa data = dm.retrieveData();
		String index = "NIM--009";
		// for (Mahasiswa mahasiswa : mhs) {
			
		// }
		for (int i = 0; i < mhs.size(); i++) {
			if (mhs.get(i).getNim() == index) {
				mhs.remove(i);
				return "Berhasil";
			} 
		}
		return "Gagal";
	}
	public String pilihan(int pil) {
		if (pil == 1) {
			tambahMahasiswa();
		} else if (pil == 2) {
			hapusMahasiswa();
//			hapusMahasiswa(hapusUntukMahasiswa());
		} else {
			System.exit(0);
		}
		
		return "Berhasil";
	}
	
	public boolean verifyName(String name) {
		String regex = "[a-zA-Z]+";
		if (Pattern.compile(regex).matcher(name).find()) {
//			model.setNama(name);
			return true;
		} else {
			return false;
		}
	}
	public boolean verifyNIM(String nim) {
		String regex = "NIM--[0-9]{3}";
		if (Pattern.compile(regex).matcher(nim).find()) {
//			model.setNim(nim);
			return true;
		} else {
			return false;
		}
	}
	public boolean verifyAge(int age) {
		if (age > 17) {
//			model.setUmur(age);
			return true;
		} 
		return false;
	}
	public String hapusUntukMahasiswa() {
		System.out.print("Masukan pilihan : ");
		String pil = input.nextLine();
		return pil;
	}
}
